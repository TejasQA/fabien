---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.epics.test.ts
---
import { data } from "./<%= h.inflection.pluralize(namespace) %>.mocks"
import { epics } from "./<%= h.inflection.pluralize(namespace) %>.epics"
import { ActionsObservable, StateObservable } from "redux-observable"
import { <%= actions.map(action => h.actionName(action, namespace)).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.actions"
import { RootState } from ".."

describe("store/<%= h.inflection.pluralize(namespace) %>.epics", () => {
  const state$: any = {
    value: {
      projects: {
        currentId: "projectId",
      },
      tenants: {
        currentId: "tenantId",
      },
    },
  }

  <% if (actionsUrl.list) { %>
  describe("list <%= h.inflection.pluralize(namespace) %>", () => {
    it("should do return <%= h.constant(h.actionName("list", namespace))%>_DONE on success", () => {
      const api: any = {
        <%= h.actionName("list", namespace) %>: (tenantId, projectId) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          return Promise.resolve(data)
        },
      }

      const action$ = ActionsObservable.of(<%= h.actionName("list", namespace) %>.started())

      return epics
        .<%= h.actionName("list", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("list", namespace) %>.done({ result: data })))
    })

    it("should do return <%= h.constant(h.actionName("list", namespace))%>_FAILED on error", () => {
      const api: any = {
        <%= h.actionName("list", namespace) %>: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(<%= h.actionName("list", namespace) %>.started())

      return epics
        .<%= h.actionName("list", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("list", namespace) %>.failed({ error: "oh no" })))
    })
  })

  <% } if (actionsUrl.get) { %>
  describe("get <%= namespace %>", () => {
    it("should do return <%= h.constant(h.actionName("get", namespace))%>_DONE on success", () => {
      const api: any = {
        <%= h.actionName("get", namespace) %>: (tenantId, projectId, <%= namespace %>Id: string) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(<%= namespace %>Id).toBe(data[0].id)
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(<%= h.actionName("get", namespace) %>.started({ id: data[0].id }))

      return epics
        .<%= h.actionName("get", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("get", namespace) %>.done({ params: { id: data[0].id }, result: data[0] })))
    })

    it("should do return <%= h.constant(h.actionName("get", namespace))%>_FAILED on error", () => {
      const api: any = {
        <%= h.actionName("get", namespace) %>: (tenantId, projectId, <%= namespace %>Id: string) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(<%= namespace %>Id).toBe(data[0].id)
          return Promise.reject("oh no")
        },
      }

      const action$ = ActionsObservable.of(<%= h.actionName("get", namespace) %>.started({ id: data[0].id }))

      return epics
        .<%= h.actionName("get", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("get", namespace) %>.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })

  <% } if (actionsUrl.update) { %>
  describe("update <%= namespace %>", () => {
    it("should do return <%= h.constant(h.actionName("update", namespace))%>_DONE on success", () => {
      const api: any = {
        <%= h.actionName("update", namespace) %>: (tenantId, projectId, <%= namespace %>) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(<%= namespace %>).toEqual({ id: data[0].id, name: "toto" })
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(<%= h.actionName("update", namespace) %>.started({ id: data[0].id, name: "toto" }))

      return epics
        .<%= h.actionName("update", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action =>
          expect(action).toEqual(<%= h.actionName("update", namespace) %>.done({ params: { id: data[0].id, name: "toto" }, result: data[0] })),
        )
    })

    it("should do return <%= h.constant(h.actionName("update", namespace))%>_FAILED on error", () => {
      const api: any = {
        <%= h.actionName("update", namespace) %>: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(<%= h.actionName("update", namespace) %>.started({ id: data[0].id }))

      return epics
        .<%= h.actionName("update", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("update", namespace) %>.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })

  <% } if (actionsUrl.delete) { %>
  describe("delete <%= namespace %>", () => {
    it("should do return <%= h.constant(h.actionName("delete", namespace))%>_DONE on success", () => {
      const api: any = {
        <%= h.actionName("delete", namespace) %>: (tenantId, projectId, <%= namespace %>Id) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(<%= namespace %>Id).toEqual(data[0].id)
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(<%= h.actionName("delete", namespace) %>.started({ id: data[0].id }))

      return epics
        .<%= h.actionName("delete", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("delete", namespace) %>.done({ params: { id: data[0].id }, result: data[0] })))
    })

    it("should do return <%= h.constant(h.actionName("delete", namespace))%>_FAILED on error", () => {
      const api: any = {
        <%= h.actionName("delete", namespace) %>: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(<%= h.actionName("delete", namespace) %>.started({ id: data[0].id }))

      return epics
        .<%= h.actionName("delete", namespace) %>(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(<%= h.actionName("delete", namespace) %>.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })
  <% } %>
})
