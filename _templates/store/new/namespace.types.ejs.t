---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.types.ts
---
/**
 * <%= h.pascal(namespace) %> from the server response
 */
export interface Raw<%= h.pascal(namespace) %> {
  <% h.toDefinition(params).forEach(param => { %>
  <%= param.name %><%= param.optionnal ? "?" : "" %>: <%= param.type %>
  <% }) %>
}

/**
 * <%= h.pascal(namespace) %> with client side sugar
 */
export interface <%= h.pascal(namespace) %> extends Raw<%= h.pascal(namespace) %> {
  /**
   * `true` if the <%= namespace %> is modify client side without backend response
   */
  isDirty?: boolean
  /**
   * `true` when the client send delete and wait for the backend response
   */
  isDeleted?: boolean
  /**
   * `true` if an error occured on update or delete
   */
  isError?: boolean
}

<% if (actions.includes("get")) { %>
export interface Get<%= h.pascal(namespace) %>Payload {
  id: <%= h.pascal(namespace) %>["id"]
}

<% } %>
<% if (actions.includes("create")) { %>
// @todo make the type more accurate
export type Create<%= h.pascal(namespace) %>Payload = Partial<%- `<${h.pascal(namespace)}>` %>

<% } %>
<% if (actions.includes("update")) { %>
export type Update<%= h.pascal(namespace) %>Payload = Partial<%- `<${h.pascal(namespace)}>` %> & { id: <%= h.pascal(namespace) %>["id"] }

<% } %>
<% if (actions.includes("delete")) { %>
export interface Delete<%= h.pascal(namespace) %>Payload {
  id: <%= h.pascal(namespace) %>["id"]
}

<% } %>
<% if (actions.includes("list")) { %>
export interface List<%= h.pascal(namespace) %>sResponse {
  data: Raw<%= h.pascal(namespace) %>[]
  page: {
    last: number
    count: number
  }
}

<% } %>