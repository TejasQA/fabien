// see types of prompts:
// https://github.com/SBoudrias/Inquirer.js#prompt-types
//
// and for examples for prompts:
// https://github.com/SBoudrias/Inquirer.js/tree/master/examples

const types = ["string", "string!", "number", "number!"]
const params = Array(10)
  .fill(0)
  .reduce(
    (mem, _, i) => [
      ...mem,
      {
        type: "input",
        name: `params.${i}.name`,
        message: "What's the name of your param?",
        when: answers => answers.next !== false,
      },
      {
        type: "list",
        name: `params.${i}.type`,
        message: "What's the type of your param?",
        choices: types,
        when: answers => answers.next !== false,
      },
      {
        type: "confirm",
        name: "next",
        message: "Have you another param?",
        default: true,
        when: answers => answers.next !== false,
      },
    ],
    [],
  )

const actionUrl = name => [
  {
    type: "input",
    name: `actionsUrl.${name}`,
    message: `What's the API route for ${name}`,
    default: answers =>
      answers.actionsUrl &&
      (answers.actionsUrl.update ||
        answers.actionsUrl.get ||
        answers.actionsUrl.delete ||
        answers.actionsUrl.create ||
        answers.actionsUrl.list),
    when: answers => answers.actions.includes(name),
  },
]

module.exports = [
  {
    type: "input",
    name: "namespace",
    message: "What's your namespace?",
    default: "bundle",
  },
  {
    type: "checkbox",
    name: "actions",
    message: "What actions do you need?",
    choices: [
      { name: "list" },
      { name: "get" },
      { name: "create" },
      { name: "update" },
      { name: "delete" },
    ],
  },
  ...actionUrl("list"),
  ...actionUrl("get"),
  ...actionUrl("create"),
  ...actionUrl("update"),
  ...actionUrl("delete"),
  ...params,
]
