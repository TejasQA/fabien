---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.reducer.test.ts
---
import { reducer, initialState } from "./<%= h.inflection.pluralize(namespace) %>.reducer"
import { <%= actions.map(action => h.actionName(action, namespace)).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.actions"
import { data } from "./<%= h.inflection.pluralize(namespace) %>.mocks"

describe("store/<%= h.inflection.pluralize(namespace) %>.reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, { type: "" })).toEqual(initialState)
  })
  <% if (actionsUrl.list) { %>
  describe("list <%= h.inflection.pluralize(namespace) %> started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, <%- h.actionName("list", namespace) %>.started()).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.started()).error).toBe(null)
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.started()).data).toEqual(data)
    })
  })

  describe("list <%= h.inflection.pluralize(namespace) %> done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.done({ result: data })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.done({ result: data })).error).toBe(null)
    })
    it("should update all the data", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.done({ result: data })).data).toEqual(data)
    })
  })

  describe("list <%= h.inflection.pluralize(namespace) %> failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.failed({ error: "oh no" })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.failed({ error: "oh no" })).error).toBe("oh no")
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, <%- h.actionName("list", namespace) %>.failed({ error: "oh no" })).data).toEqual(data)
    })
  })
  
  <% } if (actionsUrl.get) { %>
  describe("get <%= namespace %> started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, <%- h.actionName("get", namespace) %>.started({ id: "abc" })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.started({ id: "abc" })).error).toBe(null)
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.started({ id: "abc" })).data).toEqual(data)
    })
  })

  describe("get <%= namespace %> done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should add the <%= namespace %> to current data if not exists (empty data)", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.done({ result: data[0], params: { id: "abc" } })).data).toEqual([data[0]])
    })
    it("should add the <%= namespace %> to current data if not exists (existing data)", () => {
      const state = {
        ...initialState,
        loading: 1,
        data,
      }
      const result = { ...data[0], id: "new" }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.done({ result, params: { id: "new" } })).data).toEqual([...data, result])
    })
    it("should update the <%= namespace %> if it's already in data", () => {
      const state = {
        ...initialState,
        loading: 1,
        data,
      }
      const result = { ...data[0], name: "new" }

      const newState = reducer(state, <%- h.actionName("get", namespace) %>.done({ result, params: { id: data[0].id } }))
      expect(newState.data[0].name).toBe("new")
      expect(newState.data[1].name).toBe(data[1].name)
      expect(newState.data.length).toBe(2)
    })
  })

  describe("get <%= namespace %> failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, <%- h.actionName("get", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).data).toEqual(data)
    })
  })

  <% } if (actionsUrl.update) { %>
  describe("update <%= namespace %> started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, <%- h.actionName("update", namespace) %>.started({ id: data[0].id })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("update", namespace) %>.started({ id: data[0].id })).error).toBe(null)
    })
    it("should set the <%= namespace %> to isDirty (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, <%- h.actionName("update", namespace) %>.started({ id: data[0].id }))
      expect(nextState.data[0].isDirty).toBe(true)
      expect(nextState.data[1].isDirty).toBe(undefined)
    })
    it("should combine updated values with the actual <%= namespace %> (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, <%- h.actionName("update", namespace) %>.started({ id: data[0].id, name: "client only" }))
      expect(nextState.data[0].name).toBe("client only")
      expect(nextState.data[0].branch).toBe(data[0].branch)
    })
  })

  describe("update <%= namespace %> done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("update", namespace) %>.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("update", namespace) %>.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should update the <%= namespace %> with the last result", () => {
      const state = {
        ...initialState,
        loading: 1,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            updatedAt: "now",
          },
        ],
      }
      const result = {
        ...data[1],
        updatedAt: "2 minutes ago",
      }

      expect(reducer(state, <%- h.actionName("update", namespace) %>.done({ result, params: { id: data[1].id } })).data).toEqual([data[0], result])
    })
  })

  describe("update <%= namespace %> failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("update", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("update", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should set the isError flags to the failed <%= namespace %>", () => {
      const state = {
        ...initialState,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            updatedAt: "now",
          },
        ],
      }

      const nextState = reducer(state, <%- h.actionName("update", namespace) %>.failed({ error: "oh no", params: { id: data[1].id } }))
      expect(nextState.data).toEqual([
        data[0],
        {
          ...data[1],
          isDirty: true,
          isError: true,
          updatedAt: "now",
        },
      ])
    })
  })

  <% } if (actionsUrl.delete) { %>
  describe("delete <%= namespace %> started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, <%- h.actionName("delete", namespace) %>.started({ id: data[0].id })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("delete", namespace) %>.started({ id: data[0].id })).error).toBe(null)
    })
    it("should set the <%= namespace %> to isDirty (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, <%- h.actionName("delete", namespace) %>.started({ id: data[0].id }))
      expect(nextState.data[0].isDirty).toBe(true)
      expect(nextState.data[1].isDirty).toBe(undefined)
    })
    it("should set the <%= namespace %> to isDeleted (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, <%- h.actionName("delete", namespace) %>.started({ id: data[0].id }))
      expect(nextState.data[0].isDeleted).toBe(true)
      expect(nextState.data[1].isDeleted).toBe(undefined)
    })
  })

  describe("delete <%= namespace %> done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("delete", namespace) %>.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, <%- h.actionName("delete", namespace) %>.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should remove the <%= namespace %> from the data", () => {
      const state = {
        ...initialState,
        loading: 1,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            isDeleted: true,
          },
        ],
      }

      expect(reducer(state, <%- h.actionName("delete", namespace) %>.done({ result: data[1], params: { id: data[1].id } })).data).toEqual([data[0]])
    })
  })

  describe("delete <%= namespace %> failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("delete", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, <%- h.actionName("delete", namespace) %>.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should set the isError flag to the failed <%= namespace %>", () => {
      const state = {
        ...initialState,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            isDeleted: true,
          },
        ],
      }

      const nextState = reducer(state, <%- h.actionName("delete", namespace) %>.failed({ error: "oh no", params: { id: data[1].id } }))
      expect(nextState.data).toEqual([
        data[0],
        {
          ...data[1],
          isDirty: true,
          isDeleted: true,
          isError: true,
        },
      ])
    })
  })
  <% } %>
})
