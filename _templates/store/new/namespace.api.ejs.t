---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.api.ts
---
import { <%= h.pascal(namespace) %>, Raw<%= h.pascal(namespace) %>, <%= actionsUrl.list ? `List${h.pascal(namespace)}sResponse, ` : "" %> <%= actions.filter(action => ["update", "create"].includes(action)).map(action => h.actionPayload(action, namespace)).join(", ") %> } from "./<%= namespace %>s.types"
import { transform } from "./<%= h.inflection.pluralize(namespace) %>.adapter"
import { api } from "../api"

<% if (actionsUrl.list) { %>
/**
 * List the available <%= h.inflection.pluralize(namespace) %> for the project
 *
 * @param tenantId
 * @param projectId
 */
export const list<%= h.pascal(namespace) %>s = (tenantId: string, projectId: string): Promise<%- "<" + h.pascal(namespace) %>[]> =>
  api
    .get<List<%= h.pascal(namespace) %>sResponse>(`${tenantId}/api/v1/realms/${projectId}/<%= actionsUrl.list %>s`)
    .then(({ data }) => data.map(transform))

<% } %>
<% if (actionsUrl.get) { %>
/**
 * Retrieve a specific <%= namespace %> in the project
 *
 * @param tenantId
 * @param projectId
 * @param <%= namespace %>Id
 */
export const get<%= h.pascal(namespace) %> = (tenantId: string, projectId: string, <%= namespace %>Id: <%= h.pascal(namespace) %>["id"]): Promise<%- "<" + h.pascal(namespace) %> > =>
  api.get<Raw<%= h.pascal(namespace) %>>(`${tenantId}/api/v1/realms/${projectId}/<%- actionsUrl.get %>`).then(transform)

<% } %>
<% if (actionsUrl.create) { %>
/**
 * Create a <%= namespace %> in the project
 *
 * @param tenantId
 * @param projectId
 * @param <%= namespace %>
 */
export const create<%= h.pascal(namespace) %> = (tenantId: string, projectId: string, <%= namespace %>: Create<%= h.pascal(namespace) %>Payload): Promise<%- "<" + h.pascal(namespace) %>> =>
  api
    .post<Raw<%= h.pascal(namespace) %>, <%= h.actionPayload("create", namespace) %>>(`${tenantId}/api/v1/realms/${projectId}/<%- actionsUrl.create %>`, <%= namespace %>)
    .then(transform)

<% } %>
<% if (actionsUrl.update) { %>
/**
 * Update a specific <%= namespace %> in the project
 *
 * @param tenantId
 * @param projectId
 * @param <%= namespace %>
 */
export const update<%= h.pascal(namespace) %> = (tenantId: string, projectId: string, <%= namespace %>: Update<%= h.pascal(namespace) %>Payload): Promise<%- "<" + h.pascal(namespace) %>>  =>
  api
    .patch<Raw<%= h.pascal(namespace) %>, <%= h.actionPayload("update", namespace) %>>(`${tenantId}/api/v1/realms/${projectId}/<%- actionsUrl.update %>`, <%= namespace %>)
    .then(transform)

<% } %>
<% if (actionsUrl.delete) { %>
/**
 * Delete a specific <%= namespace %> in the project
 *
 * @param tenantId
 * @param projectId
 * @param <%= namespace %>Id
 */
export const delete<%= h.pascal(namespace) %> = (tenantId: string, projectId: string, <%= namespace %>Id: <%= h.pascal(namespace) %>["id"]): Promise<%- "<" + h.pascal(namespace) %>> =>
  api.delete<Raw<%= h.pascal(namespace) %>>(`${tenantId}/api/v1/realms/${projectId}/<%= actionsUrl.delete %>`).then(transform)

<% } %>