---
inject: true
to: src/store/index.ts
skip_if: <%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.reducer
after: "// Reducers import"
---
import { <%= h.pascal(namespace) %>sState, reducer as <%= h.inflection.pluralize(namespace) %> } from "./<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.reducer"