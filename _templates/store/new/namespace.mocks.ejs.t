---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.mocks.ts
---
import { <%= h.pascal(namespace) %>, Raw<%= h.pascal(namespace) %> } from "./<%= h.inflection.pluralize(namespace) %>.types"

export const data: <%= h.pascal(namespace) %>[] = [
  {
    <% h.toDefinition(params).forEach(param => { %>
    <%= param.name %>: <%- param.type === "string" ? `"${h.casual.title}"` : h.casual.integer() %>
    <% }) %>
  },
  {
    <% h.toDefinition(params).forEach(param => { %>
    <%= param.name %>: <%- param.type === "string" ? `"${h.casual.title}"` : h.casual.integer() %>
    <% }) %>
  },
]

export const rawData: Raw<%= h.pascal(namespace) %>[] = [
  {
    <% h.toDefinition(params).forEach(param => { %>
    <%= param.name %>: <%- param.type === "string" ? `"${h.casual.title}"` : h.casual.integer() %>
    <% }) %>
  },
  {
    <% h.toDefinition(params).forEach(param => { %>
    <%= param.name %>: <%- param.type === "string" ? `"${h.casual.title}"` : h.casual.integer() %>
    <% }) %>
  },
]
