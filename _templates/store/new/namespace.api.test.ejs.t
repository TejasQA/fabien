---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.api.test.ts
---
import { baseUrl } from "../api"
import nock from "nock"
import { rawData, data } from "./<%= h.inflection.pluralize(namespace) %>.mocks"
import { <%= actions.map(action => h.actionName(action, namespace) ).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.api"

describe("store/<%= namespace %>.api", () => {
  afterEach(() => nock.cleanAll())

  <% if (actionsUrl.list) { %>
  describe("<%- h.actionName("list", namespace) %>", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .get("/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>")
        .reply(200, { data: rawData })

      return <%- h.actionName("list", namespace) %>("my-tenant", "my-project").then(<%= h.inflection.pluralize(namespace) %> => expect(<%= h.inflection.pluralize(namespace) %>).toEqual(data))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .get("/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>")
        .reply(500, "oh no!")

      return <%- h.actionName("list", namespace) %>("my-tenant", "my-project").catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })

  <% } if (actionsUrl.get) { %>
  describe("<%- h.actionName("get", namespace) %>", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .get(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(200, rawData[0])

      return <%- h.actionName("get", namespace) %>("my-tenant", "my-project", rawData[0].id).then(<%= namespace %> => expect(<%= namespace %>).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .get(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(500, "oh no!")

      return <%- h.actionName("get", namespace) %>("my-tenant", "my-project", rawData[0].id).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })

  <% } if (actionsUrl.create) { %>
  describe("<%- h.actionName("create", namespace) %>", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .post(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>`)
        .reply(200, rawData[0])

      return <%- h.actionName("create", namespace) %>("my-tenant", "my-project", rawData[0]).then(<%= namespace %> => expect(<%= namespace %>).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .post(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>`)
        .reply(500, "oh no!")

      return <%- h.actionName("create", namespace) %>("my-tenant", "my-project", rawData[0]).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })

  <% } if (actionsUrl.update) { %>
  describe("<%- h.actionName("update", namespace) %>", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .update(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(200, rawData[0])

      return <%- h.actionName("update", namespace) %>("my-tenant", "my-project", rawData[0].id).then(<%= namespace %> => expect(<%= namespace %>).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .update(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(500, "oh no!")

      return <%- h.actionName("update", namespace) %>("my-tenant", "my-project", rawData[0].id).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })

  <% } if (actionsUrl.delete) { %>
  describe("<%- h.actionName("delete", namespace) %>", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .delete(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(200, rawData[0])

      return <%- h.actionName("delete", namespace) %>("my-tenant", "my-project", rawData[0].id).then(<%= namespace %> => expect(<%= namespace %>).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .delete(`/my-tenant/api/v1/realms/my-project/<%= h.inflection.pluralize(namespace) %>/${rawData[0].id}`)
        .reply(500, "oh no!")

      return <%- h.actionName("delete", namespace) %>("my-tenant", "my-project", rawData[0].id).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })
  <% } %>
})
