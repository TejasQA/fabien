---
inject: true
append: true
to: src/store/api.ts
skip_if: <%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.api
---
export * from "./<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.api"