---
inject: true
to: src/store/index.ts
skip_if: <%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.epics
after: "// Epics import"
---
import { epics as bundlesEpics } from "./bundles/bundles.epics"
import { epics as <%= h.inflection.pluralize(namespace) %>Epics } from "./<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.epics"