---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.actions.test.ts
---
import { data } from "./<%= h.inflection.pluralize(namespace) %>.mocks"
import { <%= actions.map(action => h.actionName(action, namespace)).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.actions"

describe("store/<%= h.inflection.pluralize(namespace) %>.actions", () => {
<% if (actions.includes("list")) { %>
  describe("<%= h.actionName("list", namespace) %>", () => {
    it("should create the <%= h.constant(h.actionName("list", namespace))%>_STARTED action", () => {
      expect(<%= h.actionName("list", namespace) %>.started()).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("list", namespace))%>_STARTED",
        payload: undefined,
      })
    })
    it("should create the <%= h.constant(h.actionName("list", namespace))%>_DONE action", () => {
      expect(<%= h.actionName("list", namespace) %>.done({ result: data })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("list", namespace))%>_DONE",
        payload: {
          result: data,
        },
      })
    })
    it("should create a <%= h.constant(h.actionName("list", namespace))%>_FAILED action", () => {
      expect(<%= h.actionName("list", namespace) %>.failed({ error: "oh no!" })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("list", namespace))%>_FAILED",
        error: true,
        payload: {
          error: "oh no!",
        },
      })
    })
  })

<% } %>
<% if (actions.includes("get")) { %>
  describe("<%= h.actionName("get", namespace) %>", () => {
    it("should create the <%= h.constant(h.actionName("get", namespace)) %>_STARTED action", () => {
      expect(<%= h.actionName("get", namespace) %>.started({ id: "abc" })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("get", namespace)) %>_STARTED",
        payload: {
          id: "abc",
        },
      })
    })
    it("should create the <%= h.constant(h.actionName("get", namespace)) %>_DONE action", () => {
      expect(<%= h.actionName("get", namespace) %>.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("get", namespace)) %>_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a <%= h.constant(h.actionName("get", namespace)) %>_FAILED action", () => {
      expect(<%= h.actionName("get", namespace) %>.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("get", namespace)) %>_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })

<% } %>
<% if (actions.includes("update")) { %>
  describe("<%= h.actionName("update", namespace) %>", () => {
    it("should create the <%= h.constant(h.actionName("update", namespace)) %>_STARTED action", () => {
      expect(<%= h.actionName("update", namespace) %>.started({ id: "abc", name: "Fabien's Project" })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("update", namespace)) %>_STARTED",
        payload: {
          id: "abc",
          name: "Fabien's Project",
        },
      })
    })
    it("should create the <%= h.constant(h.actionName("update", namespace)) %>_DONE action", () => {
      expect(<%= h.actionName("update", namespace) %>.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("update", namespace)) %>_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a <%= h.constant(h.actionName("update", namespace)) %>_FAILED action", () => {
      expect(<%= h.actionName("update", namespace) %>.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("update", namespace)) %>_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })

<% } %>
<% if (actions.includes("delete")) { %>
  describe("<%= h.actionName("delete", namespace) %>", () => {
    it("should create the <%= h.constant(h.actionName("delete", namespace))%>_STARTED action", () => {
      expect(<%= h.actionName("delete", namespace) %>.started({ id: "abc" })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("delete", namespace))%>_STARTED",
        payload: {
          id: "abc",
        },
      })
    })
    it("should create the <%= h.constant(h.actionName("delete", namespace))%>_DONE action", () => {
      expect(<%= h.actionName("delete", namespace) %>.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("delete", namespace))%>_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a <%= h.constant(h.actionName("delete", namespace))%>_FAILED action", () => {
      expect(<%= h.actionName("delete", namespace) %>.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@<%= namespace %>s/<%= h.constant(h.actionName("delete", namespace))%>_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })
})
<% } %>
