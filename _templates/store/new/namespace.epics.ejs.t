---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.epics.ts
---
import { filter, mergeMap } from "rxjs/operators"

import { EpicFactory, RootState } from ".."
import * as actions from "./<%= h.inflection.pluralize(namespace) %>.actions"
import { StateObservable } from "redux-observable"

const getProjectId = (state$: StateObservable<RootState>) => state$.value.projects.currentId
const getTenantId = (state$: StateObservable<RootState>) => state$.value.tenants.currentId

export const epics: EpicFactory = {
  <% if (actionsUrl.list) { %>
  <%= h.actionName("list", namespace) %>: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.<%= h.actionName("list", namespace) %>.started.match),
      mergeMap(_ =>
        api
          .<%= h.actionName("list", namespace) %>(getTenantId(state$), getProjectId(state$))
          .then(result => actions.<%= h.actionName("list", namespace) %>.done({ result }))
          .catch(error => actions.<%= h.actionName("list", namespace) %>.failed({ error })),
      ),
    ),
  <% } if (actionsUrl.get) { %>
  <%= h.actionName("get", namespace) %>: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.<%= h.actionName("get", namespace) %>.started.match),
      mergeMap(action =>
        api
          .<%= h.actionName("get", namespace) %>(getTenantId(state$), getProjectId(state$), action.payload.id)
          .then(result => actions.<%= h.actionName("get", namespace) %>.done({ result, params: action.payload }))
          .catch(error => actions.<%= h.actionName("get", namespace) %>.failed({ error, params: action.payload })),
      ),
    ),

  <% } if (actionsUrl.update) { %>
  <%= h.actionName("update", namespace) %>: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.<%= h.actionName("update", namespace) %>.started.match),
      mergeMap(action =>
        api
          .<%= h.actionName("update", namespace) %>(getTenantId(state$), getProjectId(state$), action.payload)
          .then(result => actions.<%= h.actionName("update", namespace) %>.done({ result, params: action.payload }))
          .catch(error => actions.<%= h.actionName("update", namespace) %>.failed({ error, params: action.payload })),
      ),
    ),

  <% } if (actionsUrl.delete) { %>
  <%= h.actionName("delete", namespace) %>: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.<%= h.actionName("delete", namespace) %>.started.match),
      mergeMap(action =>
        api
          .<%= h.actionName("delete", namespace) %>(getTenantId(state$), getProjectId(state$), action.payload.id)
          .then(result => actions.<%= h.actionName("delete", namespace) %>.done({ result, params: action.payload }))
          .catch(error => actions.<%= h.actionName("delete", namespace) %>.failed({ error, params: action.payload })),
      ),
    ),
  <% } %>
}
