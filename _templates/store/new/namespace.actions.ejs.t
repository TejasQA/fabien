---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.actions.ts
---
import actionCreatorFactory from "typescript-fsa"
import { <%= h.pascal(namespace) %>, <%= actions.filter(a => a !== "list").map(action => h.actionPayload(action, namespace)).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.types"

const actionCreator = actionCreatorFactory("@@<%= h.inflection.pluralize(namespace) %>")
<%- actions.map(action => 
`export const ${h.actionName(action, namespace)} = actionCreator.async<${h.actionPayload(action, namespace)}, ${h.pascal(namespace)}${action === "list" ? "[]": ""}, string>("${h.constant(h.actionName(action, namespace))}")`
).join("\n") %>
