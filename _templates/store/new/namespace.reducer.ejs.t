---
to: src/store/<%= h.inflection.pluralize(namespace) %>/<%= h.inflection.pluralize(namespace) %>.reducer.ts
---
import { <%= actions.map(action => h.actionName(action, namespace)).join(", ") %> } from "./<%= h.inflection.pluralize(namespace) %>.actions"

import { isType } from "typescript-fsa"
import { Action } from "redux"
import { <%= h.pascal(namespace) %> } from "./<%= h.inflection.pluralize(namespace) %>.types"

// Initial state
export const initialState = {
  data: [] as <%= h.pascal(namespace) %>[],
  loading: 0,
  error: null as string,
}
export type <%= h.pascal(namespace) %>sState = typeof initialState

// Reducer
export const reducer = (state = initialState, action: Action): <%= h.pascal(namespace) %>sState => {
  <% if (actionsUrl.list) { %>
  // <%= h.constant(h.actionName("list", namespace)) %>
  if (isType(action, list<%= h.pascal(namespace) %>s.started)) {
    return { ...state, loading: state.loading + 1, error: null }
  }
  if (isType(action, list<%= h.pascal(namespace) %>s.done)) {
    return { ...state, loading: state.loading - 1, data: action.payload.result, error: null }
  }
  if (isType(action, list<%= h.pascal(namespace) %>s.failed)) {
    return { ...state, loading: state.loading - 1, error: action.payload.error }
  }

  <% } if (actionsUrl.get) { %>
  // <%= h.constant(h.actionName("get", namespace)) %>
  if (isType(action, get<%= h.pascal(namespace) %>.started)) {
    return { ...state, loading: state.loading + 1, error: null }
  }
  if (isType(action, get<%= h.pascal(namespace) %>.failed)) {
    return { ...state, loading: state.loading - 1, error: action.payload.error }
  }

  <% } if (actionsUrl.update) { %>
  // <%= h.constant(h.actionName("update", namespace)) %>
  if (isType(action, update<%= h.pascal(namespace) %>.started)) {
    const <%= namespace %>Id = action.payload.id
    return {
      ...state,
      loading: state.loading + 1,
      error: null,
      data: state.data.map(
        <%= namespace %> => (<%= namespace %>.id === <%= namespace %>Id ? { ...<%= namespace %>, ...action.payload, isDirty: true } : <%= namespace %>),
      ),
    }
  }
  <% } %>

  <% if (actionsUrl.get || actionsUrl.update) { %>
  // (<%= actionsUrl.get ? "GET" : "" %><%- (actionsUrl.get && actionsUrl.update) ? "|" : "" %><%= actionsUrl.update ? "UPDATE" : "" %>)_<%= h.constant(namespace) %>_DONE
  if (<%= actionsUrl.get ? `isType(action, get${h.pascal(namespace)}.done)` : "" %><%- (actionsUrl.get && actionsUrl.update) ? " || ": "" %><%- actionsUrl.update ? `isType(action, update${h.pascal(namespace)}.done)` : "" %>) {
    const <%= namespace %>Id = action.payload.params.id
    const indexOfPrevious<%= h.pascal(namespace) %> = state.data.findIndex(({ id }) => id === <%= namespace %>Id)
    return {
      ...state,
      loading: state.loading - 1,
      data:
        indexOfPrevious<%= h.pascal(namespace) %> === -1
          ? [...state.data, action.payload.result]
          : state.data.map(<%= namespace %> => (<%= namespace %>.id === <%= namespace %>Id ? action.payload.result : <%= namespace %>)),
      error: null,
    }
  }

  <% } if (actionsUrl.delete) { %>
  // <%= h.constant(h.actionName("delete", namespace)) %>
  if (isType(action, delete<%= h.pascal(namespace) %>.started)) {
    const <%= namespace %>Id = action.payload.id
    return {
      ...state,
      loading: state.loading + 1,
      error: null,
      data: state.data.map(<%= namespace %> => (<%= namespace %>.id === <%= namespace %>Id ? { ...<%= namespace %>, isDeleted: true, isDirty: true } : <%= namespace %>)),
    }
  }
  if (isType(action, delete<%= h.pascal(namespace) %>.done)) {
    const <%= namespace %>Id = action.payload.params.id
    return {
      ...state,
      loading: state.loading - 1,
      error: null,
      data: state.data.filter(({ id }) => id !== <%= namespace %>Id),
    }
  }
  <% } %>

  <% if (actionsUrl.update || actionsUrl.delete) { %>
  // (<%= actionsUrl.update ? "UPDATE" : "" %><%- (actionsUrl.update && actionsUrl.delete) ? "|" : "" %><%= actionsUrl.delete ? "DELETE" : "" %>)_<%= h.constant(namespace) %>_FAILED
  if (<%- actionsUrl.update ? `isType(action, update${h.pascal(namespace)}.failed)` : "" %><%- (actionsUrl.update && actionsUrl.delete) ? " || " : "" %><%- actionsUrl.delete ? `isType(action, delete${h.pascal(namespace)}.failed)` : "" %>) {
    const <%= namespace %>Id = action.payload.params.id
    return {
      ...state,
      loading: state.loading - 1,
      error: action.payload.error,
      data: state.data.map(<%= namespace %> => (<%= namespace %>.id === <%= namespace %>Id ? { ...<%= namespace %>, isError: true } : <%= namespace %>)),
    }
  }
  <% } %>

  return state
}
