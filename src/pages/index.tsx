import * as React from "react"
import { BrowserRouter as Router, Route } from "react-router-dom"
import { hot } from "react-hot-loader"

import Home from "./Home"

export default hot(module)(() => (
  <Router>
    <Route exact path="/" component={Home} />
  </Router>
))
