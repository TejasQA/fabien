import * as React from "react"
import { connect } from "react-redux"
import { FormattedMessage } from "react-intl"

import { Bundle } from "../store/bundles/bundles.types"
import { RootState } from "../store"
import * as actions from "../store/bundles/bundles.actions"

export interface BundleListProps {
  bundles: Bundle[]
  loading: number
  error: any // @todo define this type properly
  onBundleClick?: (bundleId: Bundle["id"]) => void
  listBundles: typeof actions.listBundles.started
}

export const BundleList: React.SFC<BundleListProps> = props => {
  const { bundles, loading, error, listBundles } = props
  const noBundle = bundles.length < 1

  return (
    <>
      <button onClick={() => listBundles()}>Fetch</button>
      {error && error.message}
      {loading > 0 && <FormattedMessage id="loading" defaultMessage="Loading…" />}
      <table>
        <thead>
          <tr>
            <th>
              <FormattedMessage id="bundles.id" defaultMessage="id" />
            </th>
            <th>
              <FormattedMessage id="bundles.name" defaultMessage="name" />
            </th>
          </tr>
        </thead>
        <tbody>
          {noBundle && (
            <tr>
              <td>
                <FormattedMessage
                  id="bundles.empty"
                  defaultMessage="No bundle here :("
                  description="Message to show when the user have no bundle"
                />
              </td>
            </tr>
          )}
          {bundles.map(({ id, name }) => (
            <tr key={id}>
              <td>{id}</td>
              <td>{name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}

BundleList.defaultProps = {
  bundles: [],
}

const mapStateToProps = ({ bundles: { data, loading, error } }: RootState) => ({ bundles: data, loading, error })
const mapDispatchToProps = { listBundles: actions.listBundles.started }

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BundleList)
