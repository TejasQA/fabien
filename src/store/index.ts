import { Epic, createEpicMiddleware, combineEpics } from "redux-observable"
import { Action, createStore, applyMiddleware, combineReducers } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"

import * as api from "./api"

// Reducers import
import { BundlesState, reducer as bundles } from "./bundles/bundles.reducer"
import { ProjectsState, reducer as projects } from "./projects/projects.reducer"
import { TenantsState, reducer as tenants } from "./tenants/tenants.reducer"

// Epics import
import { epics as bundlesEpics } from "./bundles/bundles.epics"

export interface RootState {
  bundles: BundlesState
  projects: ProjectsState
  tenants: TenantsState
}

const rootReducer = combineReducers<RootState>({ bundles, projects, tenants })

export const dependencies = {
  api,
}

export type EpicDependencies = typeof dependencies

export interface EpicFactory {
  [key: string]: Epic<Action, Action, RootState, EpicDependencies>
}

const rootEpic = combineEpics(...Object.values(bundlesEpics))

const epicMiddleware = createEpicMiddleware<Action, Action, RootState, EpicDependencies>({ dependencies })

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(epicMiddleware)))

epicMiddleware.run(rootEpic)
