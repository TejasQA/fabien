import { reducer, initialState } from "./projects.reducer"
import { projects } from "./projects.mocks"
import { setProjects } from "./projects.actions"

describe("store/projects.reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, { type: "" })).toEqual(initialState)
  })

  describe("set projects", () => {
    it("should set the new projects into data", () => {
      expect(reducer(undefined, setProjects({ projects })).data).toEqual(
        projects,
      )
    })
    it("should set the first projects as current", () => {
      expect(reducer(undefined, setProjects({ projects })).currentId).toEqual(
        projects[0].id,
      )
    })
  })
})
