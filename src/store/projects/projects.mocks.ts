import { Project } from "./projects.types"

export const projects: Project[] = [
  {
    id: "4c495436-4973-5138-bb06-7a304df61932",
    name: "Contiamo",
  },
  {
    id: "cfaecfaf-3464-544a-abd9-892bf1d3c454",
    name: "Frontends",
  },
]
