import actionCreatorFactory from "typescript-fsa"
import { Project } from "./projects.types"

const actionCreator = actionCreatorFactory("@@projects")
export const setProjects = actionCreator<{ projects: Project[] }>(
  "SET_PROJECTS",
)
