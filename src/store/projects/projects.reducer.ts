import { setProjects } from "./projects.actions"
import { Action } from "redux"
import { isType } from "typescript-fsa"

// Initial state
export const initialState = {
  data: [],
  loading: 0,
  error: null,
  currentId: "4c495436-4973-5138-bb06-7a304df61932",
}
export type ProjectsState = typeof initialState

export const reducer = (state = initialState, action: Action) => {
  if (isType(action, setProjects)) {
    return {
      ...state,
      data: action.payload.projects,
      currentId: action.payload.projects[0].id,
    }
  }

  return state
}
