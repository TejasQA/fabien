import { setProjects } from "./projects.actions"
import { projects } from "./projects.mocks"

describe("store/projects.actions", () => {
  describe("setProjects", () => {
    it("should create the SET_PROJECTS action", () => {
      expect(setProjects({ projects })).toEqual({
        type: "@@projects/SET_PROJECTS",
        payload: { projects },
      })
    })
  })
})
