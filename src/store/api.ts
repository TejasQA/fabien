import request, { RequestPromise } from "request-promise-native"
import * as Cookies from "js-cookies"

// Base URL of each requests
export const baseUrl = "https://labs.ctmo.io"

// Get Headers
const getHeaders = () => ({
  "x-double-cookie": Cookies.default.getItem("double-cookie"),
})

/**
 * API requests wrapper
 *
 * This wrapper must be use on every *.api.ts implementations.
 *
 * Note: the name `api` was choose to give a logic warning to any developer that
 * attempts to use these methods directly on an epic => `api.api` is forbidden in this project!
 */
export const api = {
  get: <T>(path: string): RequestPromise<T> =>
    request({
      method: "GET",
      uri: `${baseUrl}/${path}`,
      headers: getHeaders(),
      json: true,
      withCredentials: true,
    }),
  post: <T, U>(path: string, body: U): RequestPromise<T> =>
    request({
      method: "POST",
      uri: `${baseUrl}/${path}`,
      body,
      headers: getHeaders(),
      json: true,
      withCredentials: true,
    }),
  patch: <T, U>(path: string, body: U): RequestPromise<T> =>
    request({
      method: "PATCH",
      uri: `${baseUrl}/${path}`,
      headers: getHeaders(),
      body,
      json: true,
      withCredentials: true,
    }),
  delete: <T>(path: string): RequestPromise<T> =>
    request({
      method: "DELETE",
      uri: `${baseUrl}/${path}`,
      headers: getHeaders(),
      json: true,
      withCredentials: true,
    }),
}

// Re-export all **/*.api.ts
export * from "./bundles/bundles.api"
