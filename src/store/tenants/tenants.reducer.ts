import { Action } from "redux"

// Initial state
export const initialState = {
  data: [],
  loading: 0,
  error: null,
  currentId: "21b899ae-3eba-56a7-97da-fa26c050d1c5",
}
export type TenantsState = typeof initialState

export const reducer = (state = initialState, action: Action) => {
  return state
}
