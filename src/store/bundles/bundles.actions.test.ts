import { data } from "./bundles.mocks"
import { listBundles, getBundle, updateBundle, deleteBundle } from "./bundles.actions"

describe("store/bundles.actions", () => {
  describe("listBundles", () => {
    it("should create the LIST_BUNDLES_STARTED action", () => {
      expect(listBundles.started()).toEqual({
        type: "@@bundles/LIST_BUNDLES_STARTED",
        payload: undefined,
      })
    })
    it("should create the LIST_BUNDLES_DONE action", () => {
      expect(listBundles.done({ result: data })).toEqual({
        type: "@@bundles/LIST_BUNDLES_DONE",
        payload: {
          result: data,
        },
      })
    })
    it("should create a LIST_BUNDLES_FAILED action", () => {
      expect(listBundles.failed({ error: "oh no!" })).toEqual({
        type: "@@bundles/LIST_BUNDLES_FAILED",
        error: true,
        payload: {
          error: "oh no!",
        },
      })
    })
  })

  describe("getBundle", () => {
    it("should create the GET_BUNDLE_STARTED action", () => {
      expect(getBundle.started({ id: "abc" })).toEqual({
        type: "@@bundles/GET_BUNDLE_STARTED",
        payload: {
          id: "abc",
        },
      })
    })
    it("should create the GET_BUNDLE_DONE action", () => {
      expect(getBundle.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@bundles/GET_BUNDLE_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a GET_BUNDLE_FAILED action", () => {
      expect(getBundle.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@bundles/GET_BUNDLE_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })

  describe("updateBundle", () => {
    it("should create the UPDATE_BUNDLE_STARTED action", () => {
      expect(updateBundle.started({ id: "abc", name: "Fabien's Project" })).toEqual({
        type: "@@bundles/UPDATE_BUNDLE_STARTED",
        payload: {
          id: "abc",
          name: "Fabien's Project",
        },
      })
    })
    it("should create the UPDATE_BUNDLE_DONE action", () => {
      expect(updateBundle.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@bundles/UPDATE_BUNDLE_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a UPDATE_BUNDLE_FAILED action", () => {
      expect(updateBundle.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@bundles/UPDATE_BUNDLE_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })

  describe("deleteBundle", () => {
    it("should create the DELETE_BUNDLE_STARTED action", () => {
      expect(deleteBundle.started({ id: "abc" })).toEqual({
        type: "@@bundles/DELETE_BUNDLE_STARTED",
        payload: {
          id: "abc",
        },
      })
    })
    it("should create the DELETE_BUNDLE_DONE action", () => {
      expect(deleteBundle.done({ result: data[0], params: { id: "abc" } })).toEqual({
        type: "@@bundles/DELETE_BUNDLE_DONE",
        payload: {
          result: data[0],
          params: {
            id: "abc",
          },
        },
      })
    })
    it("should create a DELETE_BUNDLE_FAILED action", () => {
      expect(deleteBundle.failed({ error: "oh no!", params: { id: "abc" } })).toEqual({
        type: "@@bundles/DELETE_BUNDLE_FAILED",
        error: true,
        payload: {
          error: "oh no!",
          params: {
            id: "abc",
          },
        },
      })
    })
  })
})
