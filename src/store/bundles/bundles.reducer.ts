import { listBundles, getBundle, updateBundle, deleteBundle } from "./bundles.actions"

import { isType } from "typescript-fsa"
import { Action } from "redux"
import { Bundle } from "./bundles.types"

// @todo try to refactor with https://github.com/dphilipson/typescript-fsa-reducers

// Initial state
export const initialState = {
  data: [] as Bundle[],
  loading: 0,
  error: null as string,
}
export type BundlesState = typeof initialState

// Reducer
export const reducer = (state = initialState, action: Action): BundlesState => {
  // LIST_BUNDLES
  if (isType(action, listBundles.started)) {
    return { ...state, loading: state.loading + 1, error: null }
  }
  if (isType(action, listBundles.done)) {
    return { ...state, loading: state.loading - 1, data: action.payload.result, error: null }
  }
  if (isType(action, listBundles.failed)) {
    return { ...state, loading: state.loading - 1, error: action.payload.error }
  }

  // GET_BUNDLE
  if (isType(action, getBundle.started)) {
    return { ...state, loading: state.loading + 1, error: null }
  }
  if (isType(action, getBundle.failed)) {
    return { ...state, loading: state.loading - 1, error: action.payload.error }
  }

  // UPDATE_BUNDLE
  if (isType(action, updateBundle.started)) {
    const bundleId = action.payload.id
    return {
      ...state,
      loading: state.loading + 1,
      error: null,
      data: state.data.map(
        bundle => (bundle.id === bundleId ? { ...bundle, ...action.payload, isDirty: true } : bundle),
      ),
    }
  }

  // (GET|UPDATE)_BUNDLE_DONE
  if (isType(action, getBundle.done) || isType(action, updateBundle.done)) {
    const bundleId = action.payload.params.id
    const indexOfPreviousBundle = state.data.findIndex(({ id }) => id === bundleId)
    return {
      ...state,
      loading: state.loading - 1,
      data:
        indexOfPreviousBundle === -1
          ? [...state.data, action.payload.result]
          : state.data.map(bundle => (bundle.id === bundleId ? action.payload.result : bundle)),
      error: null,
    }
  }

  // DELETE_BUNDLE
  if (isType(action, deleteBundle.started)) {
    const bundleId = action.payload.id
    return {
      ...state,
      loading: state.loading + 1,
      error: null,
      data: state.data.map(bundle => (bundle.id === bundleId ? { ...bundle, isDeleted: true, isDirty: true } : bundle)),
    }
  }
  if (isType(action, deleteBundle.done)) {
    const bundleId = action.payload.params.id
    return {
      ...state,
      loading: state.loading - 1,
      error: null,
      data: state.data.filter(({ id }) => id !== bundleId),
    }
  }

  // (UPDATE|DELETE)_BUNDLE_FAILED
  if (isType(action, updateBundle.failed) || isType(action, deleteBundle.failed)) {
    const bundleId = action.payload.params.id
    return {
      ...state,
      loading: state.loading - 1,
      error: action.payload.error,
      data: state.data.map(bundle => (bundle.id === bundleId ? { ...bundle, isError: true } : bundle)),
    }
  }

  return state
}
