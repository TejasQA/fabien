import { Omit } from "type-zoo"

/**
 * Bundle from the server response
 */
export interface RawBundle {
  id: string
  name: string
  createdAt: string
  updatedAt: string
  syncedAt?: string
  deployedAt?: string
  deployedSHA?: string
  gitUrl: string
  gitSHA: string
  branch: string
  publicKey: string
  coverImageURL: string
  tenantId: string
  realmId: string
  activeDeployID?: string
  description?: string
}

/**
 * Bundle with client side sugar
 */
export interface Bundle extends Omit<RawBundle, "realmId"> {
  /**
   * New name of `realmId`
   */
  projectId: string
  /**
   * `true` if the bundle is modify client side without backend response
   */
  isDirty?: boolean
  /**
   * `true` when the client send delete and wait for the backend response
   */
  isDeleted?: boolean
  /**
   * `true` if an error occured on update or delete
   */
  isError?: boolean
}

export interface FetchBundlePayload {
  id: Bundle["id"]
}

export type UpdateBundlePayload = Partial<Bundle> & { id: Bundle["id"] }

export interface DeleteBundlePayload {
  id: Bundle["id"]
}

export interface FetchBundlesResponse {
  data: RawBundle[]
  page: {
    last: number
    count: number
  }
}
