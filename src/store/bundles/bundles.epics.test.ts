import { data } from "./bundles.mocks"
import { epics } from "./bundles.epics"
import { ActionsObservable, StateObservable } from "redux-observable"
import { listBundles, getBundle, updateBundle, deleteBundle } from "./bundles.actions"
import { RootState } from ".."

describe("store/bundles.epics", () => {
  const state$: any = {
    value: {
      projects: {
        currentId: "projectId",
      },
      tenants: {
        currentId: "tenantId",
      },
    },
  }

  describe("list bundles", () => {
    it("should do return LIST_BUNDLES_DONE on success", () => {
      const api: any = {
        listBundles: (tenantId, projectId) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          return Promise.resolve(data)
        },
      }

      const action$ = ActionsObservable.of(listBundles.started())

      return epics
        .listBundles(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(listBundles.done({ result: data })))
    })

    it("should do return LIST_BUNDLES_FAILED on error", () => {
      const api: any = {
        listBundles: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(listBundles.started())

      return epics
        .listBundles(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(listBundles.failed({ error: "oh no" })))
    })
  })

  describe("get bundle", () => {
    it("should do return GET_BUNDLE_DONE on success", () => {
      const api: any = {
        getBundle: (tenantId, projectId, bundleId: string) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(bundleId).toBe(data[0].id)
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(getBundle.started({ id: data[0].id }))

      return epics
        .getBundle(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(getBundle.done({ params: { id: data[0].id }, result: data[0] })))
    })

    it("should do return GET_BUNDLE_FAILED on error", () => {
      const api: any = {
        getBundle: (tenantId, projectId, bundleId: string) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(bundleId).toBe(data[0].id)
          return Promise.reject("oh no")
        },
      }

      const action$ = ActionsObservable.of(getBundle.started({ id: data[0].id }))

      return epics
        .getBundle(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(getBundle.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })

  describe("update bundle", () => {
    it("should do return UPDATE_BUNDLE_DONE on success", () => {
      const api: any = {
        updateBundle: (tenantId, projectId, bundle) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(bundle).toEqual({ id: data[0].id, name: "toto" })
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(updateBundle.started({ id: data[0].id, name: "toto" }))

      return epics
        .updateBundle(action$, state$, { api })
        .toPromise()
        .then(action =>
          expect(action).toEqual(updateBundle.done({ params: { id: data[0].id, name: "toto" }, result: data[0] })),
        )
    })

    it("should do return UPDATE_BUNDLE_FAILED on error", () => {
      const api: any = {
        updateBundle: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(updateBundle.started({ id: data[0].id }))

      return epics
        .updateBundle(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(updateBundle.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })

  describe("delete bundle", () => {
    it("should do return DELETE_BUNDLE_DONE on success", () => {
      const api: any = {
        deleteBundle: (tenantId, projectId, bundleId) => {
          expect(tenantId).toBe("tenantId")
          expect(projectId).toBe("projectId")
          expect(bundleId).toEqual(data[0].id)
          return Promise.resolve(data[0])
        },
      }

      const action$ = ActionsObservable.of(deleteBundle.started({ id: data[0].id }))

      return epics
        .deleteBundle(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(deleteBundle.done({ params: { id: data[0].id }, result: data[0] })))
    })

    it("should do return DELETE_BUNDLE_FAILED on error", () => {
      const api: any = {
        deleteBundle: () => Promise.reject("oh no"),
      }

      const action$ = ActionsObservable.of(deleteBundle.started({ id: data[0].id }))

      return epics
        .deleteBundle(action$, state$, { api })
        .toPromise()
        .then(action => expect(action).toEqual(deleteBundle.failed({ params: { id: data[0].id }, error: "oh no" })))
    })
  })
})
