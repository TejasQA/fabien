import { reducer, initialState } from "./bundles.reducer"
import { listBundles, getBundle, updateBundle, deleteBundle } from "./bundles.actions"
import { data } from "./bundles.mocks"

describe("store/bundles.reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, { type: "" })).toEqual(initialState)
  })

  describe("list bundles started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, listBundles.started()).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, listBundles.started()).error).toBe(null)
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, listBundles.started()).data).toEqual(data)
    })
  })

  describe("list bundles done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, listBundles.done({ result: data })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, listBundles.done({ result: data })).error).toBe(null)
    })
    it("should update all the data", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, listBundles.done({ result: data })).data).toEqual(data)
    })
  })

  describe("list bundles failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, listBundles.failed({ error: "oh no" })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, listBundles.failed({ error: "oh no" })).error).toBe("oh no")
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, listBundles.failed({ error: "oh no" })).data).toEqual(data)
    })
  })

  describe("get bundle started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, getBundle.started({ id: "abc" })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, getBundle.started({ id: "abc" })).error).toBe(null)
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, getBundle.started({ id: "abc" })).data).toEqual(data)
    })
  })

  describe("get bundle done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, getBundle.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, getBundle.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should add the bundle to current data if not exists (empty data)", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, getBundle.done({ result: data[0], params: { id: "abc" } })).data).toEqual([data[0]])
    })
    it("should add the bundle to current data if not exists (existing data)", () => {
      const state = {
        ...initialState,
        loading: 1,
        data,
      }
      const result = { ...data[0], id: "new" }
      expect(reducer(state, getBundle.done({ result, params: { id: "new" } })).data).toEqual([...data, result])
    })
    it("should update the bundle if it's already in data", () => {
      const state = {
        ...initialState,
        loading: 1,
        data,
      }
      const result = { ...data[0], name: "new" }

      const newState = reducer(state, getBundle.done({ result, params: { id: data[0].id } }))
      expect(newState.data[0].name).toBe("new")
      expect(newState.data[1].name).toBe(data[1].name)
      expect(newState.data.length).toBe(2)
    })
  })

  describe("get bundle failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, getBundle.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, getBundle.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should not erase the current data", () => {
      const state = {
        ...initialState,
        data,
      }
      expect(reducer(state, getBundle.failed({ error: "oh no", params: { id: "abc" } })).data).toEqual(data)
    })
  })

  describe("update bundle started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, updateBundle.started({ id: data[0].id })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, updateBundle.started({ id: data[0].id })).error).toBe(null)
    })
    it("should set the bundle to isDirty (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, updateBundle.started({ id: data[0].id }))
      expect(nextState.data[0].isDirty).toBe(true)
      expect(nextState.data[1].isDirty).toBe(undefined)
    })
    it("should combine updated values with the actual bundle (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, updateBundle.started({ id: data[0].id, name: "client only" }))
      expect(nextState.data[0].name).toBe("client only")
      expect(nextState.data[0].branch).toBe(data[0].branch)
    })
  })

  describe("update bundle done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, updateBundle.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, updateBundle.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should update the bundle with the last result", () => {
      const state = {
        ...initialState,
        loading: 1,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            updatedAt: "now",
          },
        ],
      }
      const result = {
        ...data[1],
        updatedAt: "2 minutes ago",
      }

      expect(reducer(state, updateBundle.done({ result, params: { id: data[1].id } })).data).toEqual([data[0], result])
    })
  })

  describe("update bundle failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, updateBundle.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, updateBundle.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should set the isError flags to the failed bundle", () => {
      const state = {
        ...initialState,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            updatedAt: "now",
          },
        ],
      }

      const nextState = reducer(state, updateBundle.failed({ error: "oh no", params: { id: data[1].id } }))
      expect(nextState.data).toEqual([
        data[0],
        {
          ...data[1],
          isDirty: true,
          isError: true,
          updatedAt: "now",
        },
      ])
    })
  })

  describe("delete bundle started", () => {
    it("should increment loading counter", () => {
      expect(reducer(undefined, deleteBundle.started({ id: data[0].id })).loading).toBe(1)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        error: "oh no!",
      }
      expect(reducer(state, deleteBundle.started({ id: data[0].id })).error).toBe(null)
    })
    it("should set the bundle to isDirty (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, deleteBundle.started({ id: data[0].id }))
      expect(nextState.data[0].isDirty).toBe(true)
      expect(nextState.data[1].isDirty).toBe(undefined)
    })
    it("should set the bundle to isDeleted (optimistic ui)", () => {
      const state = {
        ...initialState,
        data,
      }
      const nextState = reducer(state, deleteBundle.started({ id: data[0].id }))
      expect(nextState.data[0].isDeleted).toBe(true)
      expect(nextState.data[1].isDeleted).toBe(undefined)
    })
  })

  describe("delete bundle done", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, deleteBundle.done({ result: data[0], params: { id: "abc" } })).loading).toBe(0)
    })
    it("should clear the error", () => {
      const state = {
        ...initialState,
        loading: 1,
        error: "oh no!",
      }
      expect(reducer(state, deleteBundle.done({ result: data[0], params: { id: "abc" } })).error).toBe(null)
    })
    it("should remove the bundle from the data", () => {
      const state = {
        ...initialState,
        loading: 1,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            isDeleted: true,
          },
        ],
      }

      expect(reducer(state, deleteBundle.done({ result: data[1], params: { id: data[1].id } })).data).toEqual([data[0]])
    })
  })

  describe("delete bundle failed", () => {
    it("should decrement loading counter", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, deleteBundle.failed({ error: "oh no", params: { id: "abc" } })).loading).toBe(0)
    })
    it("should set the error", () => {
      const state = {
        ...initialState,
        loading: 1,
      }
      expect(reducer(state, deleteBundle.failed({ error: "oh no", params: { id: "abc" } })).error).toBe("oh no")
    })
    it("should set the isError flag to the failed bundle", () => {
      const state = {
        ...initialState,
        data: [
          data[0],
          {
            ...data[1],
            isDirty: true,
            isDeleted: true,
          },
        ],
      }

      const nextState = reducer(state, deleteBundle.failed({ error: "oh no", params: { id: data[1].id } }))
      expect(nextState.data).toEqual([
        data[0],
        {
          ...data[1],
          isDirty: true,
          isDeleted: true,
          isError: true,
        },
      ])
    })
  })
})
