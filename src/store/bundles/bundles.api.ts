import { Bundle, UpdateBundlePayload, RawBundle, FetchBundlesResponse } from "./bundles.types"
import { transform } from "./bundles.adapter"
import { api } from "../api"

/**
 * List the available bundles for the project
 *
 * @param tenantId
 * @param projectId
 */
export const listBundles = (tenantId: string, projectId: string): Promise<Bundle[]> =>
  api
    .get<FetchBundlesResponse>(`${tenantId}/api/v1/realms/${projectId}/bundles`)
    .then(({ data }) => data.map(transform))

/**
 * Retrieve a specific bundle in the project
 *
 * @param tenantId
 * @param projectId
 * @param bundleId
 */
export const getBundle = (tenantId: string, projectId: string, bundleId: Bundle["id"]): Promise<Bundle> =>
  api.get<RawBundle>(`${tenantId}/api/v1/realms/${projectId}/bundles/${bundleId}`).then(transform)

/**
 * Update a specific bundle in the project
 *
 * @param tenantId
 * @param projectId
 * @param bundle
 */
export const updateBundle = (tenantId: string, projectId: string, bundle: UpdateBundlePayload): Promise<Bundle> =>
  api
    .patch<RawBundle, UpdateBundlePayload>(`${tenantId}/api/v1/realms/${projectId}/bundles/${bundle.id}`, bundle)
    .then(transform)

/**
 * Delete a specific bundle in the project
 *
 * @param tenantId
 * @param projectId
 * @param bundleId
 */
export const deleteBundle = (tenantId: string, projectId: string, bundleId: Bundle["id"]): Promise<Bundle> =>
  api.delete<RawBundle>(`${tenantId}/api/v1/realms/${projectId}/bundles/${bundleId}`).then(transform)
