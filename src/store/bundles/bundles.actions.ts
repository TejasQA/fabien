import actionCreatorFactory from "typescript-fsa"
import { Bundle, FetchBundlePayload, UpdateBundlePayload, DeleteBundlePayload } from "./bundles.types"

const actionCreator = actionCreatorFactory("@@bundles")
export const listBundles = actionCreator.async<void, Bundle[], string>("LIST_BUNDLES")
export const getBundle = actionCreator.async<FetchBundlePayload, Bundle, string>("GET_BUNDLE")
export const updateBundle = actionCreator.async<UpdateBundlePayload, Bundle, string>("UPDATE_BUNDLE")
export const deleteBundle = actionCreator.async<DeleteBundlePayload, Bundle, string>("DELETE_BUNDLE")
