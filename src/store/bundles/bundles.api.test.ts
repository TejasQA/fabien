import { baseUrl } from "../api"
import nock from "nock"
import { rawData, data } from "./bundles.mocks"
import { listBundles, getBundle, deleteBundle } from "./bundles.api"

describe("store/bundle.api", () => {
  afterEach(() => nock.cleanAll())

  describe("listBundles", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .get("/my-tenant/api/v1/realms/my-project/bundles")
        .reply(200, { data: rawData })

      return listBundles("my-tenant", "my-project").then(bundles => expect(bundles).toEqual(data))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .get("/my-tenant/api/v1/realms/my-project/bundles")
        .reply(500, "oh no!")

      return listBundles("my-tenant", "my-project").catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })

  describe("getBundle", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .get(`/my-tenant/api/v1/realms/my-project/bundles/${rawData[0].id}`)
        .reply(200, rawData[0])

      return getBundle("my-tenant", "my-project", rawData[0].id).then(bundle => expect(bundle).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .get(`/my-tenant/api/v1/realms/my-project/bundles/${rawData[0].id}`)
        .reply(500, "oh no!")

      return getBundle("my-tenant", "my-project", rawData[0].id).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })

  describe("deleteBundle", () => {
    it("should return the correct transformed response", () => {
      nock(baseUrl)
        .delete(`/my-tenant/api/v1/realms/my-project/bundles/${rawData[0].id}`)
        .reply(200, rawData[0])

      return deleteBundle("my-tenant", "my-project", rawData[0].id).then(bundle => expect(bundle).toEqual(data[0]))
    })

    it("should return an error", () => {
      nock(baseUrl)
        .delete(`/my-tenant/api/v1/realms/my-project/bundles/${rawData[0].id}`)
        .reply(500, "oh no!")

      return deleteBundle("my-tenant", "my-project", rawData[0].id).catch(err => {
        expect(err.statusCode).toEqual(500)
        expect(err.message).toEqual('500 - "oh no!"')
        expect(err.error).toEqual("oh no!")
      })
    })
  })
})
