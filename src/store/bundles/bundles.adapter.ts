import { RawBundle, Bundle } from "./bundles.types"
import omit from "lodash/omit"

export const transform = (raw: RawBundle): Bundle => ({
  ...omit(raw, "realmId"),
  projectId: raw.realmId,
})
