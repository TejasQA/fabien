import { filter, mergeMap } from "rxjs/operators"

import { EpicFactory, RootState } from ".."
import * as actions from "./bundles.actions"
import { StateObservable } from "redux-observable"

const getProjectId = (state$: StateObservable<RootState>) => state$.value.projects.currentId
const getTenantId = (state$: StateObservable<RootState>) => state$.value.tenants.currentId

export const epics: EpicFactory = {
  listBundles: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.listBundles.started.match),
      mergeMap(_ =>
        api
          .listBundles(getTenantId(state$), getProjectId(state$))
          .then(result => actions.listBundles.done({ result }))
          .catch(error => actions.listBundles.failed({ error })),
      ),
    ),

  getBundle: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.getBundle.started.match),
      mergeMap(action =>
        api
          .getBundle(getTenantId(state$), getProjectId(state$), action.payload.id)
          .then(result => actions.getBundle.done({ result, params: action.payload }))
          .catch(error => actions.getBundle.failed({ error, params: action.payload })),
      ),
    ),

  updateBundle: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.updateBundle.started.match),
      mergeMap(action =>
        api
          .updateBundle(getTenantId(state$), getProjectId(state$), action.payload)
          .then(result => actions.updateBundle.done({ result, params: action.payload }))
          .catch(error => actions.updateBundle.failed({ error, params: action.payload })),
      ),
    ),

  deleteBundle: (action$, state$, { api }) =>
    action$.pipe(
      filter(actions.deleteBundle.started.match),
      mergeMap(action =>
        api
          .deleteBundle(getTenantId(state$), getProjectId(state$), action.payload.id)
          .then(result => actions.deleteBundle.done({ result, params: action.payload }))
          .catch(error => actions.deleteBundle.failed({ error, params: action.payload })),
      ),
    ),
}
