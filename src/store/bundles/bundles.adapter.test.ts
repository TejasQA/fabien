import { RawBundle, Bundle } from "./bundles.types"
import { transform } from "./bundles.adapter"

describe("store/bundle.adapter", () => {
  let bundle: Bundle

  beforeEach(() => {
    const raw: RawBundle = {
      branch: "branch",
      coverImageURL: "coverImageURL",
      createdAt: "yesterday",
      updatedAt: "tomorrow",
      gitSHA: "chat!",
      gitUrl: "git url",
      id: "kitten",
      name: "this is not a kitten",
      publicKey: "very secret!",
      realmId: "this should be a projectId",
      tenantId: "Contiamo",
    }

    bundle = transform(raw)
  })

  it("should replace realmId by projectId", () => {
    expect(bundle.projectId).toBe("this should be a projectId")
  })
})
