import * as React from "react"
import { Provider as ReduxProvider } from "react-redux"
import { OperationalUI } from "@operational/components"
import { Auth } from "contiamo-idp-ui"
import { IntlProvider, addLocaleData } from "react-intl"
import localeEn from "react-intl/locale-data/en"

import en from "./translations/en.json"

import { store } from "./store"
import Pages from "./pages"

addLocaleData([...localeEn])

export const App: React.SFC = () => (
  <ReduxProvider store={store}>
    <IntlProvider locale="en" messages={en}>
      <OperationalUI withBaseStyles>
        <Auth>{() => <Pages />}</Auth>
      </OperationalUI>
    </IntlProvider>
  </ReduxProvider>
)

export default App
