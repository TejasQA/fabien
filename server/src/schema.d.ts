/* tslint:disable */

export type DateTime = any

export type JSON = any

export interface Query {
  bundle?: Bundle | null
  allBundles: (Bundle | null)[]
}

export interface Bundle {
  id: string
  name: string
  createdAt: DateTime
  updatedAt: DateTime
  syncedAt?: string | null
  deployedAt?: string | null
  deployedSHA?: string | null
  gitUrl: string
  gitSHA: string
  branch: string
  publicKey: string
  coverImageURL: string
  tenantId: string
  projectId: string
  activeDeployID?: string | null
  description?: string | null
  secrets: BundleSecret[]
  editorSessions: BundleEditorSession[]
  functions: BundleFunction[]
  logs: BundleDeployLogs[]
}

export interface BundleSecret {
  name: string
  value?: string | null
  bundle: Bundle
}

export interface BundleEditorSession {
  name: string
  url?: string | null
  createdAt: DateTime
  deletedAt?: DateTime | null
  bundle: Bundle
  status?: BundleEditorSessionStatus | null
}

export interface BundleEditorSessionStatus {
  session?: BundleEditorSession | null
  connections?: number | null
  kernels?: number | null
  lastActivity?: DateTime | null
  startedAt?: DateTime | null
}

export interface BundleFunction {
  id: string
  name: string
  url: string
  deployed: boolean
  deploymentStatus?: DeployStatus | null
  updatedAt?: DateTime | null
  createdAt?: DateTime | null
}

export interface BundleDeployLogs {
  bundle: Bundle
  id: string
  createdAt: DateTime
  status: DeployStatus
  message?: string | null
}

export interface Mutation {
  createBundle?: Bundle | null /** Create a new bundle */
  updateBundle?: Bundle | null /** Update an existing bundle */
  deleteBundle?: Bundle | null /** Delete an existing bundle */
  createBundleSecret?: Bundle | null /** Create a bundle secret */
  deleteBundleSecret?: Bundle | null /** Delete a bundle secret */
  syncBundle?: Bundle | null /** Tell the remote server to sync the bundle meta in the remote git repo */
  startBundleEditorSession: BundleEditorSession /** Start a jupiterLabs editor sessionEach editor sessions is unique to the user, so multiple users can "edit" the same bundle without interfering with their other user. Their data is stored on separate volumes. */
  stopBundleEditorSession: BundleEditorSession /** Stop the editor web service. This will only stop the web backend, all of the data will be preserved.You can force it to delete the user's specific data by passing the hardReset=true parameter. */
  deployBundle: Bundle /** Trigger the build and deployment of the functions in a bundle.This will cause the Lab Server to pull a fresh copy of the bundle code from the registerd GIT repository. Once the code has been cloned, the server will then go through each function, building the corresponding docker image, and then deploying each function to the OpenFaaS cluster. */
  deleteBundleFunction: BundleFunction /** Delete a function from the bundle */
  callBundleFunction?: JSON | null /** Call a functionThe `data` and the response can't be type here, it's depends of the bundle function implementation. */
}

export interface Subscription {
  bundleEditorSessionUpdated: BundleEditorSession
}

export interface CreateBundlePayload {
  name: string
  gitUrl: string
  branch: string
  description?: string | null
}

export interface UpdateBundlePayload {
  name?: string | null
  gitUrl?: string | null
  branch?: string | null
  description?: string | null
}

export interface CreateBundleSecretPayload {
  name: string
  value: string
}
export interface BundleQueryArgs {
  tenantId: string
  projectId: string
  bundleId: string
}
export interface AllBundlesQueryArgs {
  tenantId: string
  projectId: string
}
export interface CreateBundleMutationArgs {
  tenantId: string
  projectId: string
  data: CreateBundlePayload
}
export interface UpdateBundleMutationArgs {
  tenantId: string
  projectId: string
  data: UpdateBundlePayload
}
export interface DeleteBundleMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
}
export interface CreateBundleSecretMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
  data: CreateBundleSecretPayload
}
export interface DeleteBundleSecretMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
  secretId: string
}
export interface SyncBundleMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
}
export interface StartBundleEditorSessionMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
}
export interface StopBundleEditorSessionMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
  hardReset?: boolean | null
}
export interface DeployBundleMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
}
export interface DeleteBundleFunctionMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
  functionId: string
}
export interface CallBundleFunctionMutationArgs {
  tenantId: string
  projectId: string
  bundleId: string
  functionId: string
  data?: JSON | null
}
export interface BundleEditorSessionUpdatedSubscriptionArgs {
  tenantId: string
  projectId: string
  bundleId: string
}

export enum DeployStatus {
  INITIATED = "INITIATED",
  COMPLETED = "COMPLETED",
  ERROR = "ERROR",
}
