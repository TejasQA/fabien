import { ApolloServer, PubSub } from "apollo-server"
import merge from "lodash/merge"
import { importSchema } from "@fabien0102/graphql-import"
import * as glob from "glob"

import {
  BundleQueryArgs,
  Bundle,
  AllBundlesQueryArgs,
  BundleEditorSessionUpdatedSubscriptionArgs,
  StartBundleEditorSessionMutationArgs,
  BundleEditorSession,
} from "./schema"
import { GraphQLResolveInfo } from "graphql"
import { LabsAPI } from "./services/labs/labs.datasource"
import { IncomingHttpHeaders } from "http"

// @todo fix ApolloServer `typeDefs` input type
const typeDefs: any = importSchema(`src/services/**/*.graphql`)

const resolvers = merge(
  {},
  ...glob
    .sync(`src/services/**/*.resolvers.ts`)
    .map(servicePath => require("." + servicePath.slice(3, -3)).default),
)

const pubsub = new PubSub()

const context = ({ req }) => ({
  headers: req && (req.headers as IncomingHttpHeaders),
  pubsub,
})

type ReturnType<T> = T extends (...args: any[]) => infer R ? R : never

export type Context = ReturnType<typeof context> & {
  dataSources: {
    labs: LabsAPI
  }
}

export interface Resolvers<TContext = Context, TParent = any> {
  Query: {
    bundle: (
      parent: TParent,
      args: BundleQueryArgs,
      context: TContext,
      info: GraphQLResolveInfo,
    ) => Bundle | null | Promise<Bundle | null>
    allBundles: (
      parent: TParent,
      args: AllBundlesQueryArgs,
      context: TContext,
      info: GraphQLResolveInfo,
    ) => Array<Bundle | null> | Promise<Array<Bundle | null>>
  }
  Mutation: {
    startBundleEditorSession: (
      parent: TParent,
      args: StartBundleEditorSessionMutationArgs,
      context: TContext,
      info: GraphQLResolveInfo,
    ) => BundleEditorSession | Promise<BundleEditorSession>
  }
  Subscription: {
    bundleEditorSessionUpdated: {
      subscribe: (
        parent: TParent,
        args: BundleEditorSessionUpdatedSubscriptionArgs,
        context: TContext,
        info: GraphQLResolveInfo,
      ) => AsyncIterator<BundleEditorSession>
    }
  }
}

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context,
  dataSources: () => ({
    labs: new LabsAPI(),
  }),
})

server
  .listen()
  .then(({ url }) =>
    // tslint:disable-next-line:no-console
    console.log(`🚀  Server ready at ${url}`),
  )
  .catch(console.log)
