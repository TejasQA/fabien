import { Resolvers } from "../.."
import { data } from "./labs.mocks"
import { BundleEditorSession } from "../../schema"
import { withFilter } from "apollo-server"

/**
 * Retrieve a specific bundle in the project.
 *
 * @rest {{apiBase}}/realms/{{realmId}}/bundles/{{bundleId}}
 */
export const bundle: Resolvers["Query"]["bundle"] = (
  _,
  { tenantId, projectId, bundleId },
  { dataSources: { labs } },
) => data.find(({ id }) => id === bundleId) // labs.getBundle(tenantId, projectId, bundleId)

/**
 * List the available bundles for the project.
 *
 * @rest {{apiBase}}/realms/{{realmId}}/bundles
 */
export const allBundles: Resolvers["Query"]["allBundles"] = (
  _,
  { tenantId, projectId },
  { dataSources: { labs } },
) => data // labs.listBundles(tenantId, projectId)

/**
 * Start a jupyter labs session with the current bundle
 */
export const startBundleEditorSession: Resolvers["Mutation"]["startBundleEditorSession"] = (
  _,
  { tenantId, projectId, bundleId },
  { pubsub },
) => {
  // Push the session into the bundle (after 1sc)
  setTimeout(() => {
    const session: BundleEditorSession = {
      name: "fabien session",
      bundle: data[0],
      createdAt: "",
      status: {
        startedAt: new Date().toISOString(),
        lastActivity: new Date().toISOString(),
        connections: 0,
        kernels: 1,
      },
      url: "https://fabien0102.com",
    }

    pubsub.publish(
      `START_BUNDLE_EDITOR_SESSION_${tenantId}_${projectId}_${bundleId}`,
      {
        // tslint:disable-next-line:no-object-literal-type-assertion
        bundleEditorSessionUpdated: session,
      },
    )
  }, 1000)

  // Return the session (without url or status yet)
  return Promise.resolve({
    bundle: data[0],
    createdAt: new Date().toISOString(),
    name: "fabien session",
  })
}

/**
 * Subscribe for bundle editor session update
 */
export const bundleEditorSessionUpdated: Resolvers["Subscription"]["bundleEditorSessionUpdated"] = {
  subscribe: (_, { tenantId, projectId, bundleId }, { pubsub }) =>
    pubsub.asyncIterator(
      `START_BUNDLE_EDITOR_SESSION_${tenantId}_${projectId}_${bundleId}`,
    ),
}

export default {
  Query: {
    bundle,
    allBundles,
  },
  Mutation: {
    startBundleEditorSession,
  },
  Subscription: {
    bundleEditorSessionUpdated,
  },
}
