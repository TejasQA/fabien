import { RESTDataSource } from "apollo-datasource-rest"
import omit from "lodash/omit"
import { Context } from "../.."
import { Bundle } from "../../schema"

export interface RawBundle {
  id: string
  name: string
  createdAt: string
  updatedAt: string
  syncedAt?: string
  deployedAt?: string
  deployedSHA?: string
  gitUrl: string
  gitSHA: string
  branch: string
  publicKey: string
  coverImageURL: string
  tenantId: string
  realmId: string
  activeDeployID?: string
  description?: string
}

export class LabsAPI extends RESTDataSource<Context> {
  baseURL = "https://labs.ctmo.io"

  willSendRequest(request: Request) {
    const { headers } = this.context
    if (headers["x-double-cookie"]) {
      request.headers.set("x-double-cookie", headers[
        "x-double-cookie"
      ] as string)
    }
    request.headers.set("content-type", "application/json")
  }

  /**
   * List available bundles in a project
   *
   * @param tenantId id of the tenant
   * @param projectId id of the project (also name realm)
   */
  async listBundles(tenantId: string, projectId: string): Promise<Bundle[]> {
    const res = await this.get(`${tenantId}/api/v1/realms/${projectId}/bundles`)
    return res.data.map(this.transform)
  }

  /**
   * Retrieve data of a specific bundle
   *
   * @param tenantId id of the tenant
   * @param projectId id of the project (also name realm)
   * @param bundleId id of the bundle
   */
  async getBundle(
    tenantId: string,
    projectId: string,
    bundleId: string,
  ): Promise<Bundle> {
    const res = await this.get(
      `${tenantId}/api/v1/realms/${projectId}/bundles/${bundleId}`,
    )
    return this.transform(res)
  }

  /**
   * Transform a bundle from the api to a bundle ready for client
   *
   * @param raw bundle from the api
   */
  private transform(raw: RawBundle): Bundle {
    return {
      ...omit(raw, "realmId"),
      projectId: raw.realmId,
      secrets: [],
      functions: [],
      editorSessions: [],
      logs: [],
    }
  }
}
