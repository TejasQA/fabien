scalar JSON
scalar DateTime

type Bundle {
  id: ID!
  name: String!
  createdAt: DateTime!
  updatedAt: DateTime!
  syncedAt: String
  deployedAt: String
  deployedSHA: String
  gitUrl: String!
  gitSHA: String!
  branch: String!
  publicKey: String!
  coverImageURL: String!
  tenantId: String!
  projectId: String!
  activeDeployID: String
  description: String
  secrets: [BundleSecret!]!
  editorSessions: [BundleEditorSession!]!
  functions: [BundleFunction!]!
  logs: [BundleDeployLogs!]!
}

type BundleSecret {
  name: String!
  value: String
  bundle: Bundle!
}

type BundleEditorSession {
  name: String!
  url: String
  createdAt: DateTime!
  deletedAt: DateTime
  bundle: Bundle!
  status: BundleEditorSessionStatus
}

type BundleEditorSessionStatus {
  session: BundleEditorSession
  connections: Int
  kernels: Int
  lastActivity: DateTime
  startedAt: DateTime
}

enum DeployStatus {
  INITIATED
  COMPLETED
  ERROR
}

type BundleDeployLogs {
  bundle: Bundle!
  id: ID!
  createdAt: DateTime!
  status: DeployStatus!
  message: String
}

type BundleFunction {
  id: ID!
  name: String!
  url: String!
  deployed: Boolean!
  deploymentStatus: DeployStatus
  updatedAt: DateTime
  createdAt: DateTime
}

type Query {
  bundle(tenantId: ID!, projectId: ID!, bundleId: ID!): Bundle
  allBundles(tenantId: ID!, projectId: ID!): [Bundle]!
}

input CreateBundlePayload {
  name: String!
  gitUrl: String!
  branch: String!
  description: String
}

input UpdateBundlePayload {
  name: String
  gitUrl: String
  branch: String
  description: String
}

input CreateBundleSecretPayload {
  name: String!
  value: String!
}

type Mutation {
  "Create a new bundle"
  createBundle(
    tenantId: ID!
    projectId: ID!
    data: CreateBundlePayload!
  ): Bundle
  "Update an existing bundle"
  updateBundle(
    tenantId: ID!
    projectId: ID!
    data: UpdateBundlePayload!
  ): Bundle
  "Delete an existing bundle"
  deleteBundle(tenantId: ID!, projectId: ID!, bundleId: ID!): Bundle

  "Create a bundle secret"
  createBundleSecret(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
    data: CreateBundleSecretPayload!
  ): Bundle
  "Delete a bundle secret"
  deleteBundleSecret(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
    secretId: ID!
  ): Bundle

  "Tell the remote server to sync the bundle meta in the remote git repo"
  syncBundle(tenantId: ID!, projectId: ID!, bundleId: ID!): Bundle

  """
  Start a jupiterLabs editor session

  Each editor sessions is unique to the user, so multiple users can "edit" the same bundle without interfering with their other user. Their data is stored on separate volumes.
  """
  startBundleEditorSession(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
  ): BundleEditorSession!
  """
  Stop the editor web service. This will only stop the web backend, all of the data will be preserved.
  You can force it to delete the user's specific data by passing the hardReset=true parameter.
  """
  stopBundleEditorSession(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
    hardReset: Boolean
  ): BundleEditorSession!

  """
  Trigger the build and deployment of the functions in a bundle.

  This will cause the Lab Server to pull a fresh copy of the bundle code from the registerd GIT repository. Once the code has been cloned, the server will then go through each function, building the corresponding docker image, and then deploying each function to the OpenFaaS cluster.
  """
  deployBundle(tenantId: ID!, projectId: ID!, bundleId: ID!): Bundle!

  "Delete a function from the bundle"
  deleteBundleFunction(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
    functionId: ID!
  ): BundleFunction!
  """
  Call a function

  The `data` and the response can't be type here, it's depends of the bundle function implementation.
  """
  callBundleFunction(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
    functionId: ID!
    data: JSON
  ): JSON
}

type Subscription {
  bundleEditorSessionUpdated(
    tenantId: ID!
    projectId: ID!
    bundleId: ID!
  ): BundleEditorSession!
}
