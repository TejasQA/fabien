// tslint:disable:no-console

import { importSchema } from "@fabien0102/graphql-import"
import { introspectSchema } from "apollo-codegen"
import { generate } from "graphql-code-generator"
import { writeFileSync } from "fs"

const schemaPath = `dist/schema.graphql`
const jsonPath = `dist/schema.json`
const dtsPath = `src/schema.d.ts`

// Generate the schema.json
const schema = importSchema(`src/services/**/*.graphql`)
writeFileSync(schemaPath, schema)

// Generate the introspection schema
introspectSchema(schemaPath, jsonPath)
  .then(() =>
    // Generate the schema typings
    generate({
      schema: jsonPath,
      out: dtsPath,
      template: "typescript",
      overwrite: true,
    }),
  )
  .catch(console.log.bind(console))
