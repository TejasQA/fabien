# Labs UI

An amazing and robust version of Labs.

The goal is the following, one source of truth and be the more type safe as possible, from the API to the final presentational component!

## TODO

- [x] gitignore
- [x] tslint
- [x] prettier
- [x] tsconfig
- [x] redux patterns (try https://github.com/aikoven/typescript-fsa)
- [x] api
- [] generators with hygen
- [x] auth with idp
- [x] i18n
- [] connect the router to redux

## Generators

To ensure the consistency and best practices all along the project life, a bunch of generators are living in this project.
The goal is to made this template evolve with us, avoid all the boring part of redux and testing, and keep only the funny part of coding :)

## Project organization

### `index.tsx`

Entry of the application when it's include into contiamo-ui

This exports:

- The main component as `default`
- The sidenav as `LabsSideNav`

### `standalone.tsx`

Entry of the application outside of contiamo-ui frame.

### `App.tsx`

The real entry point of the application

### Components

Any components use on the application. Each component have the following naming convention:

- `MyComponent.tsx`
- `MyComponent.test.tsx`
- `MyComponent.style.ts` (optional, depend of the size of the component)

The component must be export as `default` and as a named `const` (for the tests and consistency).

If the component is connected to the store. The presentational component will be export as a named constant and the connected version as the `default`

Example of a simple component:

```tsx
import * as React from "react"

export interface MyTitleProps {
  title: string
}

export const MyTitle: React.SFC<Props> = props => <h1>{props.title}</h1>

export default MyTitle
```

Example of a connected compoment:

```tsx
import * as React from "react"
import { connect } from "react-redux"

import { getCurrentTitle } from "../store/bundle"

export interface CurrentTitleProps {
  title: string
}

export const CurrentTitle: React.SFC<Props> = ({ title }) => <h1>{title}</h1>

export const mapStateToProps = state => ({
  title: getCurrentTitle(state),
})

export default connect(mapStateToProps)(CurrentTitle)
```

Note: Everything is export here, so if, for any reasons, I need to reuse the logic outside or test the `mapStateToProps` function, I can!

#### Badass components

Sometime, components can become badass (oh yeah). In this case, the practice is to create a folder with the component name, and re-export the component in an `index.ts` file.

This is to avoid to think how to import the component from outside.

/components
/MyBadassComponent

- index.ts
- MyBadassComponent.tsx
- MyBadassComponent.test.tsx
- MyBadassComponent.style.ts
- ARandomPrivateSubComponent.tsx

### Pages

Any components accessible by the router. These component must not be connected! The goal is to connect the smaller part as possible to optimize the composability and the performance.

Ideally, these page components are almost empty and just a dummy composition of real components from `/components`.

### Store

Everything related to redux (actions, actions-creator, reducers and epics). This folder is also organize by module.

The goal in this part is to have 100% of type safety and 100% of code coverage.

Example:

/store/bundles

- bundles.actions.ts
- bundles.actions.test.ts
- bundles.reducers.ts
- bundles.reducers.test.ts
- bundles.epics.ts
- bundles.epics.test.ts
- bundles.api.ts
- bundles.api.test.ts

Everything here must and will be pure functions:

- no mutation
- no side effects in any function

One epic can trigger only one action, no more. We want to be able to follow easily the flow in the Redux dev tools.
If we need to react, we will create another epic that listen on the output action type.

Example:
This flow:
`@@moduleName/MY_ACTION_STARTED` -> `@@moduleName/MY_ACTION_DONE` -> `@@otherModule/MY_ACTION_STARTED` -> ``@@otherModule/MY_ACTION_SUCCESS`

should be code like this:

```ts
// /!\ This is pseudo code /!\

// moduleName/moduleName.epics.ts
const myActionEpic = action$ =>
  action$.ofType(`@@moduleName/MY_ACTION_STARTED`).map(() => `@@moduleName/MY_ACTION_DONE`)

// otherModule/otherModule.epics.ts
const watchForAction = action$ =>
  action$.ofType(`@@moduleName/MY_ACTION_DONE`).map(() => `@@otherModule/MY_ACTION_STARTED`)

const myActionEpic = action$ =>
  action$.ofType(`@@otherModule/MY_ACTION_STARTED`).map(() => `@@otherModule/MY_ACTION_DONE`)
```

With this pattern, the debug stack is clear and symetric. The `watch` pattern permit to easily understand any interconnection.
Any async actions will follow this pattern: `@@moduleName/MY_ACTION_STARTED`, `@@moduleName/MY_ACTION_FAILED`, `@@moduleName/MY_ACTION_DONE` (give by typescript-fsa)

## Translations

https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-react-app-with-react-intl
