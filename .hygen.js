const caseHelpers = require("case")
const casual = require("casual")

const { pascal } = caseHelpers

module.exports = {
  helpers: {
    ...caseHelpers,
    casual,
    /** transform prompt params to nice array object */
    toDefinition: params =>
      params.map(({ name, type }) => ({
        name,
        type: type.replace("!", ""),
        optionnal: !type.includes("!"),
      })),

    /** return the action name */
    actionName: (action, namespace) =>
      action === "list"
        ? `list${pascal(namespace)}s`
        : `${action}${pascal(namespace)}`,

    /** return the action payload */
    actionPayload: (action, namespace) =>
      action === "list"
        ? `void`
        : `${pascal(action)}${pascal(namespace)}Payload`,
  },
}
